package lichess

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Lichess account information
func Account(token string) (raw map[string]interface{}) {
	var lichessTokenHeaderVal = "Bearer " + token
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://lichess.org/api/account", nil)
	req.Header.Add("Authorization", lichessTokenHeaderVal)
	resp, err := client.Do(req)
	if err != nil {
		// handle error
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &raw)
	// raw["count"] = 1
	return
}
