package lichess

import (
	"io/ioutil"
	"log"
	"net/http"
)

// Export games of a user
// Download all games of any user in PGN or ndjson format.

// Games are sorted by reverse chronological order (most recent first)

// We recommend streaming the response, for it can be very long.
// https://lichess.org/@/german11 for instance has more than 320,000 games.

// The game stream is throttled, depending on who is making the request:

// Anonymous request: 10 games per second
// OAuth2 authenticated request: 20 games per second
// Authenticated, downloading your own games: 50 games per second
func GamesUser(user string, token string) (body []byte) {
	var lichessTokenHeaderVal = "Bearer " + token
	url := "https://lichess.org/api/games/user/" + user
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", lichessTokenHeaderVal)
	// req.Header.Add("Accept", "application/x-ndjson")
	resp, err := client.Do(req)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	return

}
